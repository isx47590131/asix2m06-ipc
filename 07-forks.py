#!/usr/bin/python
#-*- coding: utf-8-*-
# isx47590131
# 07/02/2018
# Exemple fork/execv
# generar subprocesos
#---------------------------------------
import os, sys

print "inici del programa principal PARE"
pid = os.fork()

if pid != 0:
	os.wait()
	print "Programa pare", os.getpid(), pid

else:
	print "Programa fill", os.getpid(), pid
	#os.execv("/usr/bin/ls", ["/usr/bin/ls","-la","/"])
	os.execl("/usr/bin/sleep", "ls","123345")

print "Fi Programa", os.getpid()
sys.exit(0)
