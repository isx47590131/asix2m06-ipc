#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de daytime server basic
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2017-2018 Febrer 2018
# -----------------------------------------------------------------
import sys,socket
from subprocess import Popen, PIPE

#Declaracions
CMD = 'date'
HOST = ''
PORT = 5001

#creacio del socket i establiemtn de la connexio
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)

while True:
	conn, addr = s.accept()
	print 'Connected by', addr
	
	#pipe
	pipeData=Popen(CMD, stdout=PIPE, stderr=PIPE, shell=True, executable='/bin/bash')
	
	for line in pipeData.stdout:
		conn.send(line)
	
	conn.close()
sys.exit()

