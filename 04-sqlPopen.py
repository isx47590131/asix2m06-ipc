#!/usr/bin/python
#-*- coding: utf-8-*-
#isx478590131
#02/02/2018
# accedir a sql amb popen
#-----------------------------------------------------
#command = ["psql", "-qtA", "-F';'" "lab_clinic"]
#cmdRemote = "psql -h 172.17.0.2 -U postgres training"
#-----------------------------------------------------
import sys
from subprocess import Popen, PIPE
import argparse

#declaracions
clie=None
cmdRemote = "psql -h 172.17.0.2 -U postgres -qt training"

#Gestio de l'entrada 
parser=argparse.ArgumentParser(description= "Consulta interactiva" )
parser.add_argument("-c", "--client", help="llista els clients\nun o n -c(int)\nnomes un client cada cop", 
	metavar="clients",action='append') 
clie=parser.parse_args()


pipeData=Popen(cmdRemote, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)

for cli in clie.client:
	sqlQuery = "select *  from clientes where num_clie=%s;" % (cli)
	pipeData.stdin.write(sqlQuery+"\n")
	data = pipeData.stdout.readline()
	if len(data) != 1:
		print data
		data=pipeData.stdout.readline()
	else:
		sys.stderr.write("clie:%s not found\n" %(cli))
pipeData.stdin.write("\q\n")
sys.exit(0)
