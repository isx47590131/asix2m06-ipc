#!/usr/bin/python
#-*- coding: utf-8-*-
# isx47590131
# 08/02/2018
# Exemple fork/signal
#---------------------------------------
import signal, os

pid = os.fork()


if pid != 0:
	print "Programa pare", os.getpid(), pid
	while True:
		pass
	exit(0)

print "PROGRAMA FILL"

os.execvp("python", ["pythoncito", "./06-signals.py", "3"])
