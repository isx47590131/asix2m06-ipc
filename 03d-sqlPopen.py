#!/usr/bin/python
#-*- coding: utf-8-*-
#isx478590131
#01/02/2018
# accedir a sql amb popen
#-----------------------------------------------------
#command = ["psql", "-qtA", "-F';'" "lab_clinic"]
#cmdRemote = "psql -h 172.17.0.2 -U postgres training"
#-----------------------------------------------------
import sys
from subprocess import Popen, PIPE
import argparse

#Gestio de l'entrada 
parser=argparse.ArgumentParser(description= "Consulta interactiva" )
parser.add_argument('num_empl', help='Sentencia SQL a executar',metavar='sentencia SQL')
args=parser.parse_args()

#Declaracions
cmdRemote = "psql -h 172.17.0.2 -U postgres training"
sqlQuery = "select *  from oficinas where oficina=%s" %  (args.num_empl)

#primer obrir el fluxe
pipeData=Popen(cmdRemote, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)

#fer el select, llegir el fluxe de tornada i tancar el fluxe
pipeData.stdin.write(sqlQuery+"\q\n")
for line in pipeData.stdout: 
  print line,
sys.exit(0)
