#!/usr/bin/python
# isx47590131 
# 31/01/2018
# exemple popen 
import sys
from subprocess import Popen, PIPE
import argparse

parser=argparse.ArgumentParser(description= "%prog [options] [arg]\ntype %prog -h\
 or%prog --help per a mes informacio" )
parser.add_argument(dest='ruta', help='Ruta a llistar',metavar='ruta-a-llistars')
args=parser.parse_args()

options="-la"
command=["ls", options, args.ruta]
p=Popen(command, stdout=PIPE, stderr=PIPE)
for line in p.stdout: # aixo es el mateix que fer un stdout.readline()
    print line
sys.exit()
