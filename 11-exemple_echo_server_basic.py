#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de echo server basic
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2017-2018 Febrer 2018
# -----------------------------------------------------------------
import sys,socket
HOST = ''
PORT = 5001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()	# serverix per asignar una tupla a dos variables
print 'Connected by', addr
while True:
    data = conn.recv(1024)
    print data
    if not data: break
    conn.send(data)
conn.close()
sys.exit(0)
