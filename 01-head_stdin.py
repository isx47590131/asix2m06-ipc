#!/usr/bin/python
#-*- coding: utf-8-*-
#isx478590131
#07/02/2018
#head [file]
#-------------------------------
import sys
import argparse

lines = 5

#Gestip de ñ'entrada
parser=argparse.ArgumentParser(description= "%prog [options] file\ntype %prog -h or %prog --help per a mes informacio" )
parser.add_argument("-l", "--lines", help="set the number of lines(int)", type=int, metavar="lines", required=False)
args=parser.parse_args()
lines=args.lines

# Processar argument
fileIn=sys.stdin
if len(sys.argv)==2:
	fileIn=open(sys.argv[1],'r')
# Generar el llistat
lineCounter=0

for oneLine in fileIn:
	lineCounter+=1
	print oneLine,
	if lineCounter==lines: break
fileIn.close()

sys.exit(0)


