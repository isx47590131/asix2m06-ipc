#!/usr/bin/python
#-*- coding: utf-8-*-
# Echo client program
'''
# Sockets: Telnet client program
# Escola del treball de Barcelona
'''
import socket
import sys
# The remote host
HOST = '192.168.2.34'
PORT = 50007# The same port as used by the server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
try:
	command=str(raw_input('Introduce comando:'))
	while True:
		if command=='': break
		s.sendall(command) #enviar dades

		data = s.recv(1024)
		#llegir dades que venen pel socket
		while data:
			if chr(4) not in data:
				sys.stdout.write(data)
				data=s.recv(1024)
			else:
				sys.stdout.write(data[:-1])
				break
			print 'Received', repr(command),' OK'
			command=str(raw_input('Introduce comando:'))
		s.close()
except EOFError:
	s.close()
	sys.exit(0)
