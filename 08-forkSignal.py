#!/usr/bin/python
#-*- coding: utf-8-*-
# isx47590131
# 08/02/2018
# Exemple fork/signal
# un pare genera un fill i es mor
# el fill bucle infinit, pero 
# acaba als 3min
# amb sighub torna a començar els 3 min
# amb sigterm missatge (segons restants)i plega
#---------------------------------------
import signal, os, sys, argparse

def mysighup(signum,frame):
	print "Signal handler called with signal", signum
	signal.alarm(3*60)

def mysigterm(signum, frame):
	print "Signal handler called with signal", signum
	print signal.alarm(0), "fi"
	sys.exit(0)


pid = os.fork()


if pid != 0:
	print "Programa pare", os.getpid(), pid
	exit(0)


signal.signal(signal.SIGTERM,mysigterm)
signal.signal(signal.SIGHUP,mysighup)
signal.alarm(3*60)
while True:
	pass
sys.exit(0)	
