#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de nmap client
# -----------------------------------------------------------------
# isx47590131
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2017-2018 20 Febrer 2018
# -----------------------------------------------------------------
import sys, os, argparse, socket, time
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="nmap.py [-p port]")
parser.add_argument("-p", "--port", type=int, default=50001) 
args = parser.parse_args()
HOST = ""
PORT = args.port
print os.getpid()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
while True:
  conn, addr = s.accept()
  ip, port = addr
  sys.stderr.write("Connected by %s" % ip)
  fileName = "%s-%s.log" % (ip, time.strftime("%Y%m%d%H%M%s"))
  fileOut = open(fileName,"w")
  while True:
	data = conn.recv(1024)
        if not data: break
	fileOut.write(data)
  fileOut.close()
  conn.close()
sys.exit(0)
