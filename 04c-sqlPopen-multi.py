#!/usr/bin/python
#-*- coding: utf-8-*-
#isx478590131
#02/02/2018
# accedir a sql amb popen amb optparse
#-------------------------------
import sys
from subprocess import Popen, PIPE
import argparse
from optparse import OptionParser

#declaracions
clie=None
bbdd=None

#Gestio de l'entrada 
parser=OptionParser(description='%prog [options] [arg]\ntype %prog -h or %prog --help per a mes informacio')
parser.add_option("-c", "--client", help="llista els clients\nun o n -c(int)", 
	metavar="clients",action='append', type=int)
parser.add_option("-b", "--base", help="Indica la base de dades a la qual vols accedir,un -b(str) obligatori",
	metavar="base de dades", dest="base")

(options, args)=parser.parse_args()
cmdRemote = "psql -h 172.17.0.2 -U postgres -qt %s" % (options.base)
pipeData=Popen(cmdRemote, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)

for cli in options.client:
	sqlQuery = "select *  from clientes where num_clie=%s;" % (cli)
	pipeData.stdin.write(sqlQuery+"\n")
	data = pipeData.stdout.readline()
	if data != '\n':
		print data
		data=pipeData.stdout.readline()
	else:
		sys.stderr.write("clie:%s not found\n" %(cli))
pipeData.stdin.write("\q\n")
sys.exit(0)

