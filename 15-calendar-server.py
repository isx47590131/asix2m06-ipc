#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de daytime server basic
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2017-2018 Febrer 2018
# -----------------------------------------------------------------
import sys, socket, argparse, signal, os
from subprocess import Popen, PIPE

#Declaracions per default
year = 2018
CMD = 'cal %i'
HOST = ''
port = 50001
num_cli = 0

parser=argparse.ArgumentParser(description= "%prog [options] [arg]\ntype %prog -h or %prog --help per a mes informacio" )
parser.add_argument("-p", "--port", help="indica el port que utilitzaras", 
	metavar="port")

#gestio de variables d'entrada/sortida
args=parser.parse_args()
port=int(args.port)


def mysigusr1(signum,frame):
	global dades
	print "Signal handler called with signal:", signum
	print "S'ha conectat el client: ", dades 

def mysigusr2(signum,frame):
	global num_cli
	print "Signal handler called with signal:", signum
	print "number of clients: ", num_cli 

def mysigalarm(signum,frame):
	print "Signal handler called with signal:", signum
	print "killeeado"
	sys.exit()

def mysigterm(signum, frame):
	print "Signal handler called with signal:", signum
	print "aixi no plego"

def mysighup(signum, frame):
	global num_cli
	print "Signal handler called with signal:", signum
	print "reset"
	num_cli = 0
	


#Creem el signals
signal.signal(signal.SIGALRM,mysigalarm)
signal.signal(signal.SIGTERM,mysigterm)
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGHUP,mysighup)

#fem el fork
pid = os.fork()

if pid != 0:
	print "Programa pare", os.getpid(), pid
	exit(0)

print "Programa fill", os.getpid()

#creacio del socket i establiemtn de la connexio
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, port))
s.listen(1)

while True:
	#Acceptem les connexions
	conn, addr = s.accept()
	print 'Connected by', addr
	year = int(conn.recv(10))
	#fem l'ordre amb el Popen
	pipeData=Popen(CMD %year, stdout=PIPE, stderr=PIPE, shell=True, executable='/bin/bash')
	dades = addr
	for line in pipeData.stdout:
		conn.send(line)
	conn.close()
	num_cli += 1
sys.exit()
