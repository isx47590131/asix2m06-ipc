#!/usr/bin/python
#-*- coding: utf-8-*-
#isx478590131
#02/02/2018
# accedir a sql amb popen
#-------------------------------
import sys
from subprocess import Popen, PIPE
import argparse

#declaracions
clie=None
bbdd=None

#Gestio de l'entrada 
parser=argparse.ArgumentParser(description= "%prog [options] [arg]\ntype %prog -h or %prog --help per a mes informacio" )
parser.add_argument("-c", "--client", help="llista els clients\nun o n -c(int)", 
	metavar="clients",action='append', type=int, required=True)
parser.add_argument("-b", "--base", help="Indica la base de dades a la qual vols accedir,un -b(str) obligatori",
	metavar="base de dades", required=True)

#gestio de variables d'entrada/sortida
args=parser.parse_args()
clie=args.client
bbdd=args.base
cmdRemote = "psql -h 172.17.0.2 -U postgres -qt %s" % str(bbdd)
print cmdRemote
pipeData=Popen(cmdRemote, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)

#gestio de variables operadors
numLin=0
totCred=0

for cli in clie:
	sqlQuery = "select *  from clientes where num_clie=%s;" % (cli)
	pipeData.stdin.write(sqlQuery+"\n")
	data = pipeData.stdout.readline()
	if data != '\n':
		print data
		pipeData.stdout.readline()
		data = data.split()
		totCred += float(data[-1])
		numLin += 1
	else:
		sys.stderr.write("clie:%s not found\n" %(cli))
pipeData.stdin.write("\q\n")

print "Total clients: %i \t Total credit = %f" %(numLin, totCred)
sys.exit(0)

