#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de echo server basic
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2017-2018 Febrer 2018
# -----------------------------------------------------------------
import sys,socket
HOST = 'localhost'
PORT = 5001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))
s.send('Hello, world')
data = s.recv(1024) # maxim caracters de la resposta
s.close()
print 'Received', repr(data)
sys.exit(0)
