#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de nmap server
# -----------------------------------------------------------------
# isx47590131
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2017-2018 20 Febrer 2018
# -----------------------------------------------------------------
import sys, socket, argparse, signal, os
from subprocess import Popen, PIPE
#processament dels arguments i variables
parser = argparse.ArgumentParser(description="telnet-server [-p port]")
parser.add_argument("-p", "--port", default=50001)
parser.add_argument("-d", "--debug", action="store_true") 
args = parser.parse_args() 
port = args.port
HOST = ""
FI = chr(4)
#creacio del socket i establiemtn de la connexio
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, port))
s.listen(1)

while True:
	conn, addr = s.accept()
	ip, port = addr
	sys.stderr.write("Connected by %s" % ip)
	while True:
		cmd=conn.recv(1024).split()
		if args.debug: sys.stderr.write(command)
		if not cmd: break
		print cmd
		#fem l'ordre i enviem el resultat
		pipeData=Popen(cmd, stdout=PIPE, stderr=PIPE)
		for line in pipeData.stdout:
			if args.debug: sys.stderr.write(command)
			conn.sendall(line)	
        for line in pipeData.stderr:
			if args.debug: sys.stderr.write(line)
			conn.sendall(line)
        conn.sendall(FI)
	conn.close()	
s.close()
sys.exit(0)
