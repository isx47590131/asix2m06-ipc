# Exemples de argparse
# isx47590131 
# 01/02/2018
## ssh-keygen -t rsa -C "your.email@example.com" -b 4096

Exemples interactius de argparser

## Exemples basics
>>> import args
>>> parser=argparse.ArgumentParser()
>>> parser
ArgumentParser(prog='', usage=None, description=None, version=None, formatter_class=<class 'argparse.HelpFormatter'>, conflict_handler='error', add_help=True)

