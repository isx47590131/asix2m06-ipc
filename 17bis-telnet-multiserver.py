#!/usr/bin/python
#-*- coding: utf-8-*-
# Echo server multi program
'''
# Sockets: Telnet client program
# Escola del treball de Barcelona
'''
import socket, sys, select,argparse, signal, os
from subprocess import Popen, PIPE
#processament dels arguments i variables
parser = argparse.ArgumentParser(description="telnet-server [-p port]")
parser.add_argument("-p", "--port", default=50001)
parser.add_argument("-d", "--debug", action="store_true") 
args = parser.parse_args() 
PORT = int(args.port)
HOST = '192.168.1.11'
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
conns=[s]
while True:
	actius,x,y = select.select(conns,[],[])
	for actual in actius:
		if actual == s:
			conn, addr = s.accept()
			print 'Connected by', addr
			conns.append(conn)
		else:
			data = actual.recv(1024)
			if not data:
				sys.stdout.write("Client finalitzat: %s \n" % (actual))
				actual.close()
				conns.remove(actual)
			else:
				pipeData=Popen(data.split(), stdout=PIPE, stderr=PIPE)
				for line in pipeData.stdout:
					if args.debug: sys.stderr.write(command)
					conn.sendall(line)
				for line in pipeData.stderr:
					if args.debug: sys.stderr.write(line)
					conn.sendall(line)
				actual.sendall(chr(4),socket.MSG_DONTWAIT)
s.close()
sys.exit(0)
	
